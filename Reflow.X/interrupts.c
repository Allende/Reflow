/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif
uint8_t samples_count;
bool           signal_pwm;
bool           nt;
/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* High-priority service */
void __interrupt() adcInt(void)
{
  if (ADIF&&ADIE)
  {
    /* must to be wait 20 TAD */
    ADIF=0;
    samples_count++;
    if(samples_count==50){
      signal_pwm=true;
      samples_count=0;
    }
    nt=TRUE;
  }
  return;
}

void __interrupt() ccpInt(void)
{
  if ()
  {
    /* code */
  }
  return;
}

void __interrupt() uartInt(void)
{
  if ()
  {
    /* code */
  }
  return;
}


