/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "enums.h"
#include "sample.h"
/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
//#define LOAD_FILE 1

/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
***************************************************************************************************/

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/
int  pointer ( void );
void put    ( s_st_x);
int  get     ( void );

/***************************************************************************************************
lobal Variables
***************************************************************************************************/
static t_samples *p_samplex = NULL;
static t_samples *p_sampley = NULL;

/************************************************************
Function Definition
*************************************************************/

int pointerx   (void)
{
  return *p_stackx->p;
}

void putx(int s)
{
  uint8_t  i = 0;
  uint16_t a,b;

  a = *p_samplex->*p;
  *p_samplex->p= *p_samplex->s;
  *p_samplex->*p = s;

  do
  {
    b = *p_samplex->*(++p);
    *p_samplex->*p = a;
  } while (i++ < 3);
  *p_samplex->p= *p_samplex->s;
}

int getx       (int i)
{
    return *p_stackx->*(p+i);
}

s_st_x pointery(void)
{
  return *p_stacky->p;
}

void puty   (int s)
{
  uint8_t  i = 0;
  uint16_t a,b;

  a = *p_sampley->*p;
  *p_sampley->p  = *p_sampley->s
  *p_sampley->*p = s;

  do
  {
    b = *p_sampley->*(++p);
    *p_sampley->*p = a;
  } while (i++ < 2);
  *p_sampley->p= *p_sampley->s;
}

int gety    (int i)
{
  return *p_stack->*(p+i);
}

void SAMPLE_Init(void *const thisx,void *const thisy)
{

  static const t_sampleVtb1 vtblx = 
  {
    pointerx,
    putx,
    getx
  };

  static const t_sampleVtb1 vtbly =
  {
    pointery,
    puty,
    gety
  };

  p_samplex = thisx;
  CAST(t_samples, thisx)->vptr = &vtblx;
  CAST(t_samples, thisx)->v    = CAST(t_samples, thisx)->s;

  p_sampley = thisy;
  CAST(t_samples, thisy)->vptr = &vtbly;
  CAST(t_samples, thisy)->v    = CAST(t_samples, thisy)->s;
}