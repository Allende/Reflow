/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/

#define CAST(t_, m_)    ((t_ *)(m_))

/***************************************************************************************************
                                 Struct/Enums used
 ***************************************************************************************************/
typedef enum t_handles t_handles;
typedef enum t_systems t_systems;
typedef enum s_st_x s_st_x;

typedef struct t_listVtb1 t_listVtb1;
typedef struct t_list t_list;

typedef struct t_stackVtb1 t_stackVtb1;
typedef struct x_stack x_stack;

typedef struct t_profileVtb1 t_profileVtb1;
typedef struct x_profile x_profile;

typedef struct t_samplesVtb1 t_samplesVtb1;
typedef struct t_samples t_samples;

typedef struct t_stVtb1 t_stVtb1;
typedef struct t_st t_st;

typedef struct t_eventVtb1 t_eventVtb1;
typedef struct x_event x_event;

struct t_list
{
  const  t_listVtb1 *vptr;
  s_st_x *p;
  s_st_x tail[5];
};

struct x_stack
{
  const   t_stackVtb1 *vptr;
  uint8_t *p;
  uint8_t *v;
};

struct x_profile
{
  const   t_profileVtb1 *vptr;
  int     p;
  uint8_t Temp[6];
  int     Time[6];
};

struct t_samples
{
  const t_samplesVtb1 *vptr;
  uint16_t   *p;
  uint16_t   s[3];
  //int   y[2];
};

struct t_st
{
  const t_stVtb1 *vptr;
  s_st_x state; // LOAD;
};

struct t_system
{  
  const t_eventVtbl *vptr;
  t_systems event;
};

struct t_handle
{
  const t_eventVtbl *vptr;
  t_handles         event;
};

struct x_event
{
  t_handle handle;
  t_system system;
};

enum t_handles
{
  hanUP,
  hanDOWN,
  hanINTRO,
  hanOUT,
  hanEND,
  hanNONE
};

enum t_systems
{
  sysEND,
  sysSTART,
  sysSAMPLE,
  sysFAILED,
  sysNONE
};

enum s_st_x
{
  BEGIN = 0,
  LOAD,
  DEFINE,
  STORAGE,
  START,
  STOP,
  END,
  PANNEL,
  MENU,
  SET,
  PROGRESS,
  CONFIRM,
  WHAIT,
  FAILED,
  A,
  B,
  C,
  D,
  E,
  TEMP,
  GAP,
  OK,
  CANCEL,
  CLEAR,
  NONE
} ;
