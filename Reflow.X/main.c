
/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include <xc.h>
#include <proc/pic18f2550.h>
#include "init.h"
#include "mvc.h"

/***************************************************************************************************
                                 Struct/Enums used
 ***************************************************************************************************/
/***************************************************************************************************
                            global Function and variables
***************************************************************************************************/
//uint8_t samples_count=0;
//bool    signal_pwm;
//bool    nt;
extern static uint8_t samples_count;
extern bool           signal_pwm;
extern bool           nt;

/***************************************************************************************************
                            Local Declaration Function 
***************************************************************************************************/
/* keygen events */
button(x_event *);
/* system events */
system(x_event *);

int Ec_diff(t_samples *,t_samples *);

bool compare(int);

float gain_conform(int);

int Reference(int);

time(void);

update(void);

void controller(x_event *this);

void main_program(x_program *this);



/***************************************************************************************************
                            Start the main program
***************************************************************************************************/
void main()
{
    //uint8_t glcd_sample[128];
    uint16_t time = 0;

    Init_HW();
    /* Initialize the glcd before displaying anything on the lcd */
    GLCD_Init();

    // single types variables (x-mix, t-types , o-objest, p-pointers , fp-point_to_functions)
    x_profile profile;
    x_view view;
    x_model model;
    x_event event;

    // variables to MVC
    x_controller controller;

    // variables to program
    x_program program;

    x_controler_init(&controller, &model, &view, &event);
    x_program_init(&program, &model, &event);

    controller_update_data(&controller, tamb);

    while (TRUE)
    {
        /* keygen events */
        button(&event->handle);
        /* system events */
        system(&event->system);

        /* MVC Scheduler */
        menu_controler(&controller);

        /* Driver main functions */
        main_program(&program);
    }
}


/***************************************************************************************************
                            Definition Functions 
***************************************************************************************************/

button(x_event *this)
{
    this->system = nt ? SAMPLE : this->system;
}
system(x_event *this)
{
    if (nt)
    {
        this->system_set(SAMPLE);
        nt = false; // from IRQ
    }
}

int Ec_diff(t_samples *x,t_samples *y)
{
    return (By * y->get(0) + Ay * y->get(1) + Cx * x->get(0) + Bx * x->get(1) + Ax * x->get(2));
}

bool compare(int x)
{
    if (x <= (samples_count * 5))
        return TRUE;
    else
        return FALSE;
}

float gain_conform(int x)
{
    f = (float)x / ROLL;
    f = f * GAIN;

    if (f > Steps_haft)
        f = 2.5;
    else if (f < -Steps_haft)
        f = -Steps_haft;

    f += Steps_haft;
    // f*=ROLL;
    return (f);
}

int Reference(this)
{
    static int     n = 0;
    static uint8_t i = 0;
    static int     Sumtime = 0;
    static int     Sumtemp = 0;
    static int     m = 0;

    while (this->profile->Time[i]!='\0')
    {
        if( Sumtime=< this->model->time )
        {
            //to figure the slope range
            if (i>0)
            {
                m = (float)((this->profile->Temp[i]-this->profile->Temp[i-1])*FS)/this->profile->Time[i];                
                Sumtemp = this->profile->Temp[i];
                n=0;                
            }
            else
            {
                m = (float)(this->profile->Temp[i]*FS)/this->profile->Time[i];
                Sumtemp = 0;
            }
            Sumtime+= this->profile->Time[i++];
        }        
        return (int)(m*n++ + Sumtemp);
    }
    return NULL;
}

// obtain time lasted
time()
{
    static int n = 0;
    
    if (n++ == 49)
    {
        this->model->time++;
        n=0;
    }
    return;
}

// obtain temp. samples for bar display
update()
{
    static bool b = FALSE;
    static int gap = 0;
    static int ngap = 0;
    int *p;

    if (!b)
    {
        *p = this->profile->time;
        b = TRUE;
        while (*(p) != '\0')
        {
            gap += *(p++); // how time it last to finish the process
        }
        gap *= 10;
    
    }

    ngap++;

    if(ngap == gap)
    {
        this->model->v->put(TFactor(ADC_GetAdcREG()));
        ngap = 0;
    }
    return;
}

int factor(int);
/* factor convert the alinear sentor to liner*/
int factor(int a)
{
    // lessed worth of a, the curve change a slope
    if(a<128)
    {

    }else{
        // convert gain : 70/170 to 5/260;
        a= (46.7 * a)/100;
        a-=
    }
}

void controller(x_event *this)
{
    //static int m[2][3]; // samples
    static bool f=FALSE;
    if (!f){
        static t_samples x; 
        static t_samples y;
        SAMPLE_Init(&x,&y);
        f=TRUE;
    }

    if (this->system == SAMPLE)
    {
        // this->system_clear();
        //a = ADC_GetAdcREG();
        /*a must to be conformed and pass to int, not float*/
        //a = Factor(a);
        /*error signal*/
        //a= Reference()- Factor( ADC_GetAdcREG() );
        /*storage and process*/
        x->put(( Reference()- factor( ADC_GetAdcREG() )));
        //vc = Ec_diff();
        //y->put(Ec_diff(&x,&y));
        //vc = gain_conform(y->put(Ec_diff(&x,&y)));

        /*compare*/
        /*Defines the out for pin_IO PWM driver */
        if (compare(gain_conform(y->put(Ec_diff(&x,&y)))) || signal_pwm) // compare Vc and Signal IRQ
        {
            signal_pwm = FALSE;
            pwm_driver = TRUE;
        }
        else
        {
            pwm_driver = FALSE;
        }
    }
    return;
}

/***************************************************************************************************
    void main_program(void)
****************************************************************************************************
 * I/P Arguments: none
 * Return value    : none

 * description  :

    This function is used to:
        1.The figure the Y[n] each 1 second

        2.The out PWM for must to be drived.

        3.The ADC stack memories must to be updated.

        4.The Vf signal must to be adapted.

        5.The driver cooler must be activated.

        6.The buzz must to be activated on every point (stages)A,B,C.. reached anda when the process
            is ended.

        7.The buzz must to be activated on every pressed switch.

        8.The leds must to be activated on every powered plate ON and blinking on every reached
            stage and permanet ON in ended reflow process.

****************************************************************************************************/
void main_program(x_program *this)
{
    static bool run = false;

    switch (this->system)
    {
    case START:
        start_init(); // init timer count, blank vars, etc
        run = true;
        break;
    case END:
        start_ended(); // end timer count etc
        run = false;
        break;
    case SAMPLE:
        if (run)
        {
            controller(this->event); // H(n)
            update(this);                // obtain temp. samples for bar display
            time(this);                  // obtain time lasted
        }
        break;
    default:
        break;
    }

    this->system_clr();
    return;
}
