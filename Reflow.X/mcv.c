/***************************************************************************************************
    Top Level Declaration
***************************************************************************************************/
#include "lib/glcd_x.h"
#include "mvc.h"
/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/

/***************************************************************************************************
MVC Functions Declaration
***************************************************************************************************/

/************************************************************
Function Definition for VIEW
*************************************************************/
void console_display_x_begin(x_model *x, s_st_x p)
{
  static s_st_x previus = BEGIN;
  static bool init = false;
  if (previus != p)
  {
    switch (previus)
    {
    case LOAD:
      GLCD_SetCursor(LOAD_FILE, LOAD_COL);
      GLCD_Printf("STOP\n");
      break;
    case DEFINE:
      GLCD_SetCursor(DEFINE_FILE, DEFINE_COL);
      GLCD_Printf("DEFINE\n");
      break;
    case START:
      GLCD_SetCursor(START_FILE, LOAD_COL);
      GLCD_Printf("START\n");
      break;
    case STOP:
      GLCD_SetCursor(STOP_FILE, LOAD_COL);
      GLCD_Printf("STOP\n");
      break;
    }
  }
  GLCD_EnableInversion();
  switch (p)
  {
  case LOAD:
    GLCD_SetCursor(LOAD_FILE, LOAD_COL);
    GLCD_Printf("LOAD\n");
    break;
  case DEFINE:
    GLCD_SetCursor(DEFINE_FILE, DEFINE_COL);
    GLCD_Printf("DEFINE\n");
    break;
  case START:
    GLCD_SetCursor(START_FILE, LOAD_COL);
    GLCD_Printf("START\n");
    break;
  case STOP:
    GLCD_SetCursor(STOP_FILE, LOAD_COL);
    GLCD_Printf("STOP\n");
    // init=false;
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Welcome to Reflow again");
      GLCD_Printf("\n");
      GLCD_GoToLine(1);
      GLCD_EnableInversion();
      GLCD_Printf("        LOAD        \n");
      GLCD_DisableInversion();
      GLCD_SetCursor(2, 8);
      GLCD_Printf("DEFINE\n");
      GLCD_SetCursor(3, 8);
      GLCD_Printf("START\n");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);
      GLCD_Printf(CURSORS);
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  previus = p;
  return;
}

void console_display_x_load(x_model *x)
{
  printGLCD("%d\r\n", x->x);
}

void console_display_x_define(x_model *x, s_st_x p)
{
  static s_st_x previus = BEGIN;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    switch (previus)
    {
    case A:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A\n");
      break;
    case B:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B\n");
      break;
    case C:
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C\n");
      break;
    case D:
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D\n");
      break;
    case E:
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E\n");
      break;
    }
  }
  GLCD_EnableInversion();
  switch (p)
  {
  case A:
    GLCD_SetCursor(A_FILE, A_COL);
    GLCD_Printf("A\n");
    break;
  case B:
    GLCD_SetCursor(B_FILE, B_COL);
    GLCD_Printf("B\n");
    break;
  case C:
    GLCD_SetCursor(C_FILE, C_COL);
    GLCD_Printf("C\n");
    break;
  case D:
    GLCD_SetCursor(D_FILE, D_COL);
    GLCD_Printf("D\n");
    // init=false;
    break;
  case E:
    GLCD_SetCursor(E_FILE, E_COL);
    GLCD_Printf("E\n");
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Define reflow curve");
      GLCD_Printf("\n");
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_EnableInversion();
      GLCD_Printf("A\n");
      GLCD_DisableInversion();
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B\n");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C\n");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D\n");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E\n");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out >:int v:up v:down\n"
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  previus = p;
  return;
}

void console_display_x_end(x_model *x, s_st_x p)
{
  static s_st_x previus = END;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_SetCursor(END_FILE, END_COL);
    GLCD_EnableInversion();
    GLCD_Printf("       ENDED        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(BOT_FILE, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
  }
  return;
}

void console_display_x_start(x_model *x, s_st_x p)
{
  static s_st_x previus = STOP;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_Printf("\n");
    GLCD_GoToLine(1);
    GLCD_Printf("LOAD\n");
    GLCD_SetCursor(2, 8);
    GLCD_Printf("DEFINE\n");
    GLCD_SetCursor(3, 8);
    GLCD_EnableInversion();
    GLCD_Printf("        STOP        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(6, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
    Delay_1000();
  }
  return;
}

void console_display_x_stop(x_model *x, s_st_x p)
{
  static s_st_x previus = START;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_Printf("\n");
    GLCD_GoToLine(1);
    GLCD_Printf("LOAD\n");
    GLCD_SetCursor(A_FILE, A_FILE);
    GLCD_Printf("DEFINE\n");
    GLCD_SetCursor(3, 8);
    GLCD_EnableInversion();
    GLCD_Printf("       START        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(6, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
  }
  return;
}

void console_display_x_confirm(x_model *x, s_st_x p)
{
  static s_st_x previus = NONE;
  static bool init = false;
  // Clean Screen high light

  if (previus != p)
  {
    switch (previus)
    {
    case OK:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("OK\n");
      break;
    case CANCEL:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("CANCEL\n");
      break;
    }
  }

  if (previus != p)
  {
    GLCD_EnableInversion();
    switch (p)
    {
    case OK:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("OK\n");
      break;
    case CANCEL:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("CANCEL\n");
      break;
    default: //"NONE"case 1:
      if (!init)
      {
        GLCD_DisableInversion();
        GLCD_ScrollMessage(0, "END the Reflow process?");
        GLCD_Printf("\n");
        GLCD_GoToLine(1);
        GLCD_Printf("OK\n");
        GLCD_SetCursor(2, 8);
        GLCD_EnableInversion();
        GLCD_Printf("CANCEL\n");
        GLCD_DisableInversion();
        GLCD_SetCursor(6, 0);
        GLCD_Printf(BAR);
        GLCD_Printf(CURSORS);
        init++;
        previus = CANCEL;
      }
      break;
    }
  }
  return;
}

void console_display_x_pannel(x_model *x, s_st_x p)
{
  static s_st_x previus = TEMP;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    switch (previus)
    {
    case TEMP:
      GLCD_SetCursor(TEMP_FILE, TEMP_COL);
      GLCD_Printf("°C\n");
      break;
    case GAP:
      GLCD_SetCursor(GAP_FILE, GAP_COL);
      GLCD_Printf("Seg\n");
      break;
    }
  }
  GLCD_EnableInversion();
  switch (p)
  {
  case TEMP:
    GLCD_SetCursor(TEMP_FILE, TEMP_COL);
    GLCD_Printf("°C\n");
    GLCD_DisableInversion();
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf(x->x);
    break;
  case GAP:
    GLCD_SetCursor(GAP_FILE, GAP_COL);
    GLCD_Printf("Seg\n");
    GLCD_DisableInversion();
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf(x->x);
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Define reflow curve");
      GLCD_Printf("\n");
      GLCD_EnableInversion();
      GLCD_SetCursor(TEMP_FILE, TEMP_COL);
      GLCD_Printf("°C\n");
      GLCD_DisableInversion();
      GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
      GLCD_Printf("0");
      GLCD_SetCursor(GAP_FILE, GAP_COL);
      GLCD_Printf("Seg\n");
      GLCD_SetCursor(GAPX_FILE, GAPX_COL);
      GLCD_Printf("0");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out >:int v:up v:down\n"
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  while (i < length(table))
    GLCD_curve_OR(table[i][1], table[i][2]);

  previus = p;
  return;
}

void console_display_x_temp(x_model *x, s_st_x p)
{
  static s_st_x previus = NONE;
  static bool init = false;
  // Clean Screen high light
  if (previus == p)
  {
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf(x->x);
  }

  if (!init)
  {
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf("0");
    init++;
    previus = p;
  }
  return;
}

void console_display_x_gap(x_model *x, s_st_x p)
{
  static s_st_x previus = NONE;
  static bool init = false;
  // Clean Screen high light
  if (previus == p)
  {
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf(x->x);
  }

  if (!init)
  {
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf("0");
    init++;
    previus = p;
  }
  return;
}

void console_display_x_progress(x_model *x, s_st_x p)
{
  static byte previus = 0;
  static bool init = false;
  byte l, i = 0;
  //
  switch (p)
  {
  case PROGRESS:
    l = strlen(x->v);
    if ((previus != l) && (l <= NSAMPLEGLCD))
    {
      do
      {
        GLCD_hist_line(i, (x->*(v++)), INIT_LINE, END_LINE);
        i++;
      } while (i++ < l);
      previus = l;

      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A");
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E");
    }
    if (!init)
    {
      GLCD_Clear;
      GLCD_ScrollMessage(0, "Progress reflow operation");
      // GLCD_Printf("\n");
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A");
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E");

      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out"
      init++;
    }
    break;
  default:
    /*STOP SIGNAL*/
    previus = 0;
    init = false;
    break;
  }
  return;
}

/*****+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * +++++++++  Factory VIEW Function Definition  *******************
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void create_console_view(x_view *this, s_st_x f)
{
  switch (f)
  {
  case BEGIN:
    this->display->f_void_x = &console_display_x_begin;
    break;
  case LOAD:
    this->display->f_void_x = &console_display_x_load;
    break;
  case DEFINE:
    this->display->f_void_x = &console_display_x_define;
    break;
  case START:
    /* code
    /*change BEGIN menu button start to stop*/
    this->display->f_void_x = &console_display_x_start;
    break;
  case STOP:
    /* code */
    /*change BEGIN menu to request confirm stop, them change menu*/
    this->display->f_void_x = &console_display_x_stop;
    break;
  case END:
    /* code */
    /*shows a chart pront to inform end flow cycle */
    this->display->f_void_x = &console_display_x_end;
    break;
  case PANNEL:
    this->display->f_void_x = &console_display_x_pannel;
    break;
  case MENU:
    /*shows a list settings storage*/
    this->display->f_void_x = &console_display_x_menu;
    break;
  case SET:
    /*shows a chart with "OK" storage procedure*/
    this->display->f_void_x = &console_display_x_set;
    /* code */
    break;
  case FAILED:
    /*shows a chart with "FAILURE" storage procedure or other*/
    /* code */
    break;
  case PROGRESS:
    /*this it for progress operation*/
    this->display->f_void_x = &console_display_x_progress;
    break;
  case WHAIT:
    /*this it for a none one operation state*/
    break;
  default:
    break;
  }
}

/************************************************************
Function Definited  to MODEL
*************************************************************/

/************************************************************
Function Definited to CONTROLLER
*************************************************************/

void controller_update_data(x_controller *this, int x)
{
  this->model->x = x;
  this->view->display->f_void_x(&this->model, this->event);
}

void x_controler_init(x_controller *this, x_model *model, x_view *view, x_event *event)
{
  this->model = model;
  this->view  = view;
  this->event = event;
}

void menu_controler(x_controller *this)
{
  static s_st_x f = BEGIN;
  bool l = TRUE;
  switch (f)
  {
  case BEGIN:
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    this->view->level->set(f);
    switch (this->event->get())
    {
    case UP:
      switch (this->view->state->get())
      {
      case LOAD:
        if (this->view->level->get() == START)
          this->view->state->set(STOP);
        else
          this->view->state->set(START);
        break;
      case DEFINE:
        this->view->state->set(LOAD);
        break;
      case START:
        this->view->state->set(DEFINE);
        break;
      case STOP:
        this->view->state->set(DEFINE);
        break;
      }
      break;
    case DOWN:
      switch (this->view->state->get())
      {
      case LOAD:
        this->view->state->set(DEFINE);
        break;
      case DEFINE:
        if (this->view->level->get() == START)
          this->view->state->set(STOP);
        else
          this->view->state->set(START);
        break;
      case START:
        this->view->state->set(LOAD);
        break;
      case STOP:
        this->view->state->set(LOAD);
        break;
      }
      break;
    case OUT:
      /*There be no sense, have not a super class or inherance*/
      // this->view->level->clear();
      break;
    case INTRO:
      this->view->level->set(this->view->state->get());
      break;
    default:
      break;
    }
    break;
  case LOAD:
    /* bring some configuration from eeprom v[[T][time]]*/
    this->view->level->set(f);
    create_console_view(&this->view, f);
    break;
  case DEFINE:
    /* Set some configuration to eeprom v[[T][time]]*/
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    this->view->level->set(f);
    switch (this->event->get())
    {
    case UP:
      switch (this->view->state->get())
      {
      case A:
        this->view->state->set(B);
        break;
      case B:
        this->view->state->set(C);
        break;
      case C:
        this->view->state->set(D);
        break;
      case D:
        this->view->state->set(E);
        break;
      case E:
        this->view->state->set(A);
        break;
      }
      break;
    case DOWN:
      switch (this->view->state->get())
      {
      case A:
        this->view->state->set(E);
        break;
      case B:
        this->view->state->set(A);
        break;
      case C:
        this->view->state->set(B);
        break;
      case D:
        this->view->state->set(C);
        break;
      case E:
        this->view->state->set(D);
        break;
      }
      break;
    case OUT:
      this->view->level->clear();
      break;
    case INTRO:
      this->view->level->set() = this->view->state->get();
      this->view->state->set(PANNEL);
      break;
    default:
      break;
    }
    break;
  case START:
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    this->event->clear();
    this->view->level->set(PROGRESS);
    this->view->state->set(PROGRESS);
    break;
  case STOP:
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    switch (this->event->get())
    {
    case UP:
    case DOWN:
      switch (this->view->state->get())
      {
      case STOP:
        this->view->state->set(PROGRESS);
        break;
      case PROGRESS:
        this->view->state->set(STOP);
        break;
      }
      break;
    case OUT:
      break;
    case INTRO:
      switch (this->view->state->get())
      {
      case STOP:
        this->view->state->set(CONFIRM);
        break;
      case PROGRESS:
        this->view->state->set(PROGRESS);
        this->view->level->set(PROGRESS);
        break;
      }
      break;
    default:
      break;
    }
    this->event->clear();
    break;
  case PROGRESS:
    /* code */
    // Not run antil 1000 seg, but de core must to run
    // signal = time_now()-time_stamp;
    // l=(signal>1000)?true:false;
    if (l)
    {
      create_console_view(&this->view, f);
      controller_update_data(x_controller * this, null);
    }
    if (init && this->event->get_sys() == SAMPLE)
      controller_update_data(x_controller * this, null);

    if (this->event->get() == OUT)
    {
      this->event->clear();
      this->view->level->clear();
      this->view->state->set(STOP);
      l = false;
    }
    break;
  case END:
    /* System Signal */
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    if (this->event->get() == OUT)
    {
      this->event->clear();
      this->view->level->clear_all();
      this->view->state->set(BEGIN);
    }
    break;
  case A:
  case B:
  case C:
  case D:
  case E:
  case PANNEL:
    /* code */
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    switch (this->event->get())
    {
    case UP:
    case DOWN:
      switch (this->view->state->get())
      {
      case TEMP:
        this->view->state->set(GAP);
        break;
      case GAP:
        this->view->state->set(TEMP);
        break;
      }
      break;
    case OUT:
      this->view->state->set(DEFINE);
      this->view->level->clear();
      break;
    case INTRO:
      switch (this->view->state->get())
      {
      case TEMP:
        this->view->state->set(TEMP);
        this->view->level->set(TEMP);
        break;
      case GAP:
        this->view->state->set(GAP);
        this->view->level->set(GAP);
        break;
      }
      break;
    default:
      break;
    }
    break;
  case TEMP:
    /* code */
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    switch (this->event->get())
    {
    case UP:
    case DOWN:
      this->view->state->set(TEMP);
      break;
    case OUT:
      this->view->state->set(TEMP);
      this->view->level->clear(); // go to to pannel
      break;
    case INTRO:
      break;
    default:
      break;
    }
    break;
  case GAP:
    /* code */
    /* code */
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    switch (this->event->get())
    {
    case UP:
    case DOWN:
      this->view->state->set(GAP);
      break;
    case OUT:
      this->view->state->set(GAP);
      this->view->level->clear(); // go to to pannel
      break;
    case INTRO:
      break;
    default:
      break;
    }
    break;
  case MENU:
    /* code */
    break;
  case SET:
    /* code */
    break;
  case CONFIRM:
    create_console_view(&this->view, f);
    controller_update_data(x_controller * this, null);
    switch (this->event->get())
    {
    case UP:
    case DOWN:
      switch (this->view->state->get())
      {
      case OK:
        this->view->state->set(CANCEL);
        break;
      case CANCEL:
        this->view->state->set(OK);
        break;
      }
      break;
    case OUT:
      this->view->state->set(STOP);
      this->view->level->clear();
      break;
    case INTRO:
      switch (this->view->state->get())
      {
      case OK:
        this->view->state->set(END);
        this->view->level->clear();
        break;
      case CANCEL:
        this->view->state->set(STOP);
        break;
      }
      break;
    default:
      break;
    }

    /* code */
    break;
  case FAILED:
    /* code */
    break;
  case WHAIT:
    /* code */
    break;
  default:
    break;
  }
}