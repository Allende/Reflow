// DEFINES
#define LCD_H         64
#define LCD_W         128

#define FAC         (5/260)*1 // convert Vfeedback
#define TFAC        (5/260)*1 // convert VtoT°Cout
#define ROLL        10000
#define TPWM        5
#define TS          1/10
#define FS          10
#define Steps       TPWM*50
#define Steps_haft  TPWM*25
#define GAIN        (Steps_haft/.458)
#define NSAMPLEGLCD LCD_W

#define Ax  1*ROLL  // sample n-2
#define Bx  1*ROLL  // sample n-1
#define Cx  1*ROLL  // sample n
#define Ay  1*ROLL  // sample n-2
#define By  1*ROLL  // sample n-1


// MACROS
#define  FACTOR(s)   (FAC*(s))
#define  TFACTOR(s)  (TFAC*(s))

// PROTOTYPES & DEFINETIONS
void Init_HW(void);
void Init_var(void);

