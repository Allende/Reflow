/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "enums.h"
#include "List.h"
/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
//#define LOAD_FILE 1

/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
***************************************************************************************************/

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/
void set       ( s_st_x);
s_st_x get     ( void );

/***************************************************************************************************
lobal Variables
***************************************************************************************************/
static t_st *p_st=NULL;
/************************************************************
Function Definition
*************************************************************/
void set      (s_st_x st)
{
  *p_st->state = st;
}

s_st_x get    (void)
{
  return *p_st->state;
}
void LIST_Init( void *const this)
{
  static const t_listVtb1 vtbl =
  {
    set,
    get,
  };

  p_st = this;
  CAST(t_st, this)->vptr= &vtbl;
  CAST(t_st, this)->p= NULL;
  CAST(t_st, this)->*tail= BEGIN;
}