/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "List.h"
#include "Stack.h"
#include "enums.h"

/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
#define LOAD_FILE 1
#define LOAD_COL 8
#define DEFINE_FILE 2
#define DEFINE_COL 7
#define START_FILE 3
#define STOP_FILE 3
/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
 ***************************************************************************************************/

typedef struct t_ViewClass  t_ViewClass;
typedef struct t_StackClass t_StackClass;
typedef struct x_model      x_model;
typedef struct x_view       x_view;
typedef struct x_controller x_controller;

struct t_ViewClass
{
  void (*f_void_x)(x_model *, t_handle);
};

struct t_StackClass
{
  void (*f_void_p)(algo *);
};

struct x_model
{
  byte x;    // variable : temp, time. etc.
  int time;  // how last the process[seg];
  x_stack v; // samples array of glcd to view process
};

struct x_view
{
  /// @brief
  t_st   state;   // LOAD;
  t_list level; // tail[5]=[BEGIN,null,null,null,null];

  t_ViewClass display;
};

struct x_controller
{
  x_event *event;
  x_model *model;
  x_view  *view;
};

/***************************************************************************************************
                            Local Function Declaration
***************************************************************************************************/

/***************************************************************************************************
                             MVC Functions Declaration
***************************************************************************************************/

/************************************************************
    Function Definition for VIEW
*************************************************************/
void console_display_x_begin(x_model *x, s_st_x p);

void console_display_x_load(x_model *x);

void console_display_x_menu(x_model *x);

void console_display_x_set(x_model *x);

void console_display_x_define(x_model *x, s_st_x p);

void console_display_x_end(x_model *x, s_st_x p);

void console_display_x_start(x_model *x, s_st_x p);

void console_display_x_stop(x_model *x, s_st_x p);

void console_display_x_confirm(x_model *x, s_st_x p);

void console_display_x_pannel(x_model *x, s_st_x p);

void console_display_x_temp(x_model *x, s_st_x p);

void console_display_x_gap(x_model *x, s_st_x p);

void console_display_x_progress(x_model *x, s_st_x p);

/*****Factory VIEW Function Definition **********************/

void create_console_view(x_view *this, s_st_x f);

/************************************************************
    Function Definited  for MODEL
*************************************************************/

/************************************************************
    Function Definited for CONTROLLER
*************************************************************/

void controller_update_data(x_controller *this, int x);

void x_controler_init(x_controller *this, x_model *model, x_view *view, x_event *event);

void menu_controler(x_controller *this);
